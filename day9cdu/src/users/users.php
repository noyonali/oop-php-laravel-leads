<?php
namespace App\users;
use PDO;
 class users{
     public $username;
     public $useremail;
     public $userpassword;
     public $date;
     public $time;
     public function __construct()
     {
         try
         {
             $this->pdo = new PDO("mysql:host=localhost;dbname=noyon_db",'root','');
         }
         catch(PDOException $e)
         {
             echo 'Error'. $e->getMessage();
         }
     }

     public function setData($username,$useremail,$userpassword){
       echo $this->username =$username;
         echo $this->useremail =$useremail;
         echo $this->userpassword =md5($userpassword);

         date_default_timezone_set('Asia/Dhaka');
         $this->date=date("Y-m-d h:i:s");
     }
     public function dataInsert(){

         $sql = "insert into users (username,useremail,userpassword,create_at) VALUES (:username,:useremail,:userpassword,:create_at)";
         $noyon = $this->pdo->prepare($sql);
         $noyon->execute(array(
             ':username'=> $this->username,
             ':useremail'=> $this->useremail,
             ':userpassword'=> $this->userpassword,
             ':create_at'=> $this->date

         ));
         return true;
     }
     public function updateData(){
         $sql = "update users set username=:username,userpassword=:userpassword,update_at=:update_at WHERE useremail = :useremail";
         $noyon = $this->pdo->prepare($sql);
         $noyon->execute(array(
             ':username'=> $this->username,
             ':useremail'=> $this->useremail,
             ':userpassword'=> $this->userpassword,
             ':update_at'=> $this->date ,

         ));
         return true;
     }
     public function deleteData(){
         $sql = "delete from users WHERE useremail = :useremail";
         $noyon = $this->pdo->prepare($sql);
         $noyon->execute(array(
             ':useremail'=> $this->useremail,
         ));
         return true;
     }
 }


?>