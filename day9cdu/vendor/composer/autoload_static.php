<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit692b057dbf7008c4bdf728e1394105e2
{
    public static $prefixLengthsPsr4 = array (
        'V' => 
        array (
            'Vendor\\Namespace\\' => 17,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Vendor\\Namespace\\' => 
        array (
            0 => __DIR__ . '/../..' . '/',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit692b057dbf7008c4bdf728e1394105e2::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit692b057dbf7008c4bdf728e1394105e2::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
