<?php
include_once ("caloop.php");
global $nb1,$nb2,$empty,$add,$substr,$addstr,$sub,$mul,$mulstr,$divistr,$divi;
 if (isset($_POST["add"])){
     $nb1 = $_POST["nb1"];
     $nb2 = $_POST["nb2"];
     if(!empty($nb1) && !empty($nb2))
     {
         $cal = new calculator();
         $add = $cal->addGet($nb1,$nb2);
         $addstr ="summation = ";

     }else{
         $empty =  "<span style ='color:red;'>Plase Full Field</span>";
     }
 }

 //substraction
    if (isset($_POST["sub"])){
        $nb1 = $_POST["nb1"];
        $nb2 = $_POST["nb2"];
        if(!empty($nb1) && !empty($nb2))
        {
            $cal = new calculator();
            $sub = $cal->subGet($nb1,$nb2);
            $substr="substraction = ";
        }else{
            $empty =  "<span style ='color:red;'>Plase Full Field</span>";
        }
}


//multiplaction

if (isset($_POST["mul"])){
    $nb1 = $_POST["nb1"];
    $nb2 = $_POST["nb2"];
    if(!empty($nb1) && !empty($nb2))
    {
        $cal = new calculator();
        $mul = $cal->mulSet($nb1,$nb2);
        $mulstr="multiplaction = ";
    }else{
        $empty =  "<span style ='color:red;'>Plase Full Field</span>";
    }
}


//division
if (isset($_POST["div"])){
    $nb1 = $_POST["nb1"];
    $nb2 = $_POST["nb2"];
    if(!empty($nb1) && !empty($nb2))
    {
        $cal = new calculator();
        $divi = $cal->diviSet($nb1,$nb2);
        $divistr="division = ";
    }else{
        $empty =  "<span style ='color:red;'>Plase Full Field</span>";
    }
}

?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="css/bootstrap.min.css">
       
    </head>
    <body>
        
        <div class="container">
			<nav class="navbar navbar-default">
				<div class="containe-fluid">
					<div class="navbar-hader">
						<h4 style="text-align:center;">Calculator System PHP</h4>
					</div>
				</div>
			</nav>
            <div class="panel panel-defult" style="margin-left:320px;">
                <div class="panel-body">
                    <div style="max-width:350px; margin:o auto;">       
                    <form action="" method="POST">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="number"></label>
                                            <input type="number" name="nb1" value="<?php echo $nb1 ;?>" class="form-control">
                                        </div>

                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="number"></label>
                                            <input type="number" name="nb2" value="<?php echo $nb2 ;?>" class="form-control"  >
                                        </div>
                                    </div>
                                </div>
                                <button style="margin-left:30px;" type="submit" name="add" class="btn btn-success">ADD(+)</button>
                                <button  type="submit" name="sub" class="btn btn-success">SUB(-)</button>
                                <button  type="submit" name="mul" class="btn btn-success">MUL(*)</button>
                                <button  type="submit" name="div" class="btn btn-success">DIVI(/)</button>
                            </div>
                        </div>
                            <h4 style="margin-left:30px; color: deepskyblue;"><?php echo $addstr . $add; ?></h4>
                            <h4 style="margin-left:30px; color: deepskyblue;"><?php echo $substr . $sub; ?></h4>
                            <h4 style="margin-left:30px; color: deepskyblue;"><?php echo $mulstr . $mul; ?></h4>
                            <h4 style="margin-left:30px; color: deepskyblue;"><?php echo $divistr . $divi ; ?></h4>
                        <h4><?php echo $empty; ?></h4>
                    </form>
                    </div>
                </div>
            </div>
            <div class="well">
                <h3>www.noyonliveproject.com
                    <span class="pull-right">Like Us www.facebook/noyon.com</span>

                </h3>
            </div>
		</div>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
        <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script>
    </body>
</html>
