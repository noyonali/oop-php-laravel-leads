<?php
namespace App\FCI\NOYON\Gardin;
class Gardin{

    public function setGrade($grade){
        $this->grade=$grade;
        if ($grade >=80 && $grade<=100){
            $this->rmark="5.00";
        }elseif ($grade>=70 && $grade<=79){
            $this->rmark="4.00";
        }elseif ($grade>=60 && $grade<=69){
            $this->rmark="3.50";
        }elseif ($grade>=50 && $grade<=59)
        {
            $this->rmark="3.00";
        }elseif ($grade>=40 && $grade<=49)
        {
            $this->rmark="2.00";
        }elseif ($grade>=33 && $grade<=39)
        {
            $this->rmark="1.00";
        }

    }
    public function getMark(){
        return $this->rmark;
    }

    //two subject...
    public function setGrade1($grade1){
        $this->grade1=$grade1;
        if ($grade1 >=80 && $grade1<=100){
            $this->rmark1="5.00";
        }elseif ($grade1>=70 && $grade1<=79){
            $this->rmark1="4.00";
        }elseif ($grade1>=60 && $grade1<=69){
            $this->rmark1="3.50";
        }elseif ($grade1>=50 && $grade1<=59)
        {
            $this->rmark1="3.00";
        }elseif ($grade1>=40 && $grade1<=49)
        {
            $this->rmark1="2.00";
        }elseif ($grade1>=33 && $grade1<=39)
        {
            $this->rmark1="1.00";
        }

    }
    public function getMark1(){
        return $this->rmark1;
    }

    // three subject
    public function setGrade2($grade2){
        $this->grade2=$grade2;
        if ($grade2 >=80 && $grade2<=100){
            $this->rmark2="5.00";
        }elseif ($grade2>=70 && $grade2<=79){
            $this->rmark2="4.00";
        }elseif ($grade2>=60 && $grade2<=69){
            $this->rmark2="3.50";
        }elseif ($grade2>=50 && $grade2<=59)
        {
            $this->rmark2="3.00";
        }elseif ($grade2>=40 && $grade2<=49)
        {
            $this->rmark2="2.00";
        }elseif ($grade2>=33 && $grade2<=39)
        {
            $this->rmark2="1.00";
        }

    }
    public function getMark2(){
        return $this->rmark2;
    }


    // four subject
    public function setGrade3($grade3){
        $this->grade3=$grade3;
        if ($grade3 >=80 && $grade3<=100){
            $this->rmark3="5.00";
        }elseif ($grade3>=70 && $grade3<=79){
            $this->rmark3="4.00";
        }elseif ($grade3>=60 && $grade3<=69){
            $this->rmark3="3.50";
        }elseif ($grade3>=50 && $grade3<=59)
        {
            $this->rmark3="3.00";
        }elseif ($grade3>=40 && $grade3<=49)
        {
            $this->rmark3="2.00";
        }elseif ($grade3>=33 && $grade3<=39)
        {
            $this->rmark3="1.00";
        }

    }
    public function getMark3(){
        return $this->rmark3;
    }


    // add  number

    public function addSomeData($a,$b,$c,$d){
        return $a+$b+$c+$d;
    }
    public function divSomeData($c){
        return $c/4;
    }
    public function pointToGradeSet($point){
        $this->point =$point;
        if ($point>=5.00 ) {
            $this->grade = "Grade = A+";
        }elseif ($point>= 4.00) {
            $this->grade = "Grade = A";
        }elseif ($point>=3.50) {
            $this->grade = "Grade = A-";
        }elseif ($point>=3.00) {
            $this->grade = " Grade = B";
        }elseif ($point>=2.00) {
            $this->grade = "Grade = C";
        }elseif ($point>=1.00) {
            $this->grade = " Grade = D";
        }else{
            $this->grade = "Grade = F";
        }
    }
    public function pointToGradeGet(){
        return $this->grade;
    }




}




?>